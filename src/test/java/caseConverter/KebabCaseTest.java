package caseConverter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class KebabCaseTest {

    @Test
    public void getString() {
        Sentence sentence = new Sentence(Arrays.asList("yeet", "yow"));

        assertEquals("yeet-yow", sentence.getString(Cases.KEBAB_CASE));
    }

    @Test
    public void getSentence() {
        Sentence expectedSentence;
        Sentence sentence;

        // TEST 1

        expectedSentence = new Sentence(Arrays.asList("yeet", "yow"));
        sentence = Cases.KEBAB_CASE.getSentence("yeet-yow");

        assertEquals(expectedSentence, sentence);

        // TEST 2

        expectedSentence = new Sentence(new ArrayList<>());
        sentence = Cases.KEBAB_CASE.getSentence("");

        assertEquals(expectedSentence, sentence);
    }
}