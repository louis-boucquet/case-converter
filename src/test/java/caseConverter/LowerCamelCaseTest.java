package caseConverter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class LowerCamelCaseTest {

    @Test
    public void getString() {
        Sentence sentence = new Sentence(Arrays.asList("yeet", "yow"));

        assertEquals("yeetYow", sentence.getString(Cases.LOWER_CAMEL_CASE));
    }

    @Test
    public void getSentence() {
        Sentence expectedSentence;
        Sentence sentence;

        // TEST 1

        expectedSentence = new Sentence(Arrays.asList("yeet", "yow"));
        sentence = Cases.LOWER_CAMEL_CASE.getSentence("yeetYow");

        assertEquals(expectedSentence, sentence);

        // TEST 2

        expectedSentence = new Sentence(new ArrayList<>());
        sentence = Cases.LOWER_CAMEL_CASE.getSentence("");

        assertEquals(expectedSentence, sentence);
    }

}