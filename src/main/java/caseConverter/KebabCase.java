package caseConverter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KebabCase implements Case {
    @Override
    public String getString(List<String> words) {
        return words
                .stream()
                .collect(Collectors.joining("-"));
    }

    @Override
    public Sentence getSentence(String sentence) {
        String[] split = sentence
                .split("-");

        List<String> words = Stream
                .of(split)
                .filter(s -> !s.equals(""))
                .collect(Collectors.toList());

        return new Sentence(words);
    }
}
