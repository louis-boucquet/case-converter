package caseConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UpperCamelCase implements Case {
    @Override
    public String getString(List<String> words) {
        return words
                .stream()
                .map(s -> String.valueOf(Character.toUpperCase(s.charAt(0))) +
                        s.subSequence(1, s.length()))
                .collect(Collectors.joining());
    }

    @Override
    public Sentence getSentence(String sentence) {
        List<String> words = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();

        for (char c : sentence.toCharArray()) {
            if (Character.isUpperCase(c)) {
                String word = stringBuilder.toString();
                words.add(word);

                stringBuilder = new StringBuilder("" + c);
            } else  {
                stringBuilder.append(c);
            }
        }

        String word = stringBuilder.toString();
        words.add(word);

        words = words
                .stream()
                .filter(s -> !s.equals(""))
                .map(String::toLowerCase)
                .collect(Collectors.toList());

        return new Sentence(words);
    }
}
