package caseConverter;

import caseConverter.Sentence;

import java.util.List;

public interface Case {
    String getString(List<String> words);

    Sentence getSentence(String sentence);
}
