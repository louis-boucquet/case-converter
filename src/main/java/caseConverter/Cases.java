package caseConverter;

public class Cases {

    public static final Case KEBAB_CASE = new KebabCase();

    public static final Case LOWER_CAMEL_CASE = new LowerCamelCase();
    public static final Case UPPER_CAMEL_CASE = new UpperCamelCase();
    public static final Case CAMEL_CASE = UPPER_CAMEL_CASE;

    private Cases() { }
}
