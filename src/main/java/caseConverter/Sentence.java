package caseConverter;

import java.util.List;
import java.util.Objects;

public class Sentence {

    private List<String> words;

    public Sentence(List<String> words) {
        this.words = words;
    }

    public String getString(Case c) {
        return c.getString(words);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence = (Sentence) o;
        return Objects.equals(words, sentence.words);
    }

    @Override
    public int hashCode() {
        return Objects.hash(words);
    }

    @Override
    public String toString() {
        return words.toString();
    }

    public static Sentence fromString(String sentence, Case c) {
        return c.getSentence(sentence);
    }
}
