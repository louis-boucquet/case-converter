package caseConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LowerCamelCase implements Case {
    @Override
    public String getString(List<String> words) {
        if (words.size() == 0)
            return "";

        return words.get(0) + words
                .subList(1, words.size())
                .stream()
                .map(s -> String.valueOf(Character.toUpperCase(s.charAt(0))) +
                        s.subSequence(1, s.length()))
                .collect(Collectors.joining());
    }

    @Override
    public Sentence getSentence(String sentence) {
        List<String> words = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();

        for (char c : sentence.toCharArray()) {
            if (Character.isUpperCase(c)) {
                String word = stringBuilder
                        .toString()
                        .toLowerCase();
                words.add(word);

                stringBuilder = new StringBuilder("" + c);
            } else  {
                stringBuilder.append(c);
            }
        }

        String word = stringBuilder
                .toString()
                .toLowerCase();

        if (!word.equals(""))
            words.add(word);

        return new Sentence(words);
    }
}
